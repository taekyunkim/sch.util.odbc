<!-- README.md is generated from README.Rmd. Please edit that file -->
sch.util.odbc
=============

The goal of `sch.util.odbc` is to offer a set of utility functions to work with Scholastic Redshift databases in both production and test servers.

Install
=======

Setup environment variables
---------------------------

Need to set up environment variables to store

-   `RS_ID`: id
-   `RS_PW`: password
-   `RS_SERVER_PROD`: production server name
-   `RS_SERVER_TEST`: test server name

Environment variables can be specified either in the system or in R. To set up variables for R, locate and edit `.Renviron` to include:

    # Scholastic redshift setup
    RS_ID=<your_redshift_id_here>
    RS_PW=<your_redshift_password_here>
    RS_SERVER_PROD=rsedwprd.c9ugetnkr4oi.us-east-1.redshift.amazonaws.com
    RS_SERVER_TEST=rspoc.cfqnltofddfp.us-east-1.redshift.amazonaws.com

Within R, check whether the variables are set up correctly.

    Sys.getenv("RS_ID")
    Sys.getenv("RS_PW")
    Sys.getenv("RS_SERVER_PROD")
    Sys.getenv("RS_SERVER_TEST")

Install the package itself
--------------------------

    install.packages("devtools")
    devtools::install_bitbucket("taekyunkim/sch.util.odbc")

Example: `query_*()`
====================

    ## basic example code
    library(tidyverse)
    library(sch.util.odbc)

    # query from production system
    dfp <-
        "select * from proj_bookclubs.mart_bookclubs_order_item limit 100" %>%
        query_prod()
    head(dfp)

    # query from test system
    dft <-
        "select * from edwcore.t008_product_dim limit 100" %>%
        query_test()
    dim(dft)

Example: `sample_*()`
=====================

    ## basic example code
    library(tidyverse)
    library(sch.util.odbc)

    # sample 100 rows from a table in the production server
    dfsp <- sample_prod("edwcore.dim_bookfair")

    # sample 100 rows from a table in the test server
    dfst <- sample_test("edwcore.dim_bookfair")

Example: check jobs in prod and test
====================================

    library(tidyverse)
    library(sch.util.odbc)

    list_jobs_in_prod()
    list_jobs_in_test()
